<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210729172859 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $createClientApi = "CREATE TABLE IF NOT EXISTS client_api
                                (
                                    id        serial,
                                    email     varchar PRIMARY KEY,
                                    api_key   CHAR(64) NOT NULL UNIQUE,
                                    name      VARCHAR  NOT NULL,
                                    is_active BOOLEAN  NOT NULL DEFAULT TRUE
                                );";

        $createVendorList = "CREATE TABLE IF NOT EXISTS vendor_list
                                (
                                    id        serial PRIMARY KEY,
                                    key       varchar NOT NULL UNIQUE,
                                    name      VARCHAR NOT NULL,                                    
                                    addr      VARCHAR NOT NULL,
                                    is_active BOOLEAN NOT NULL DEFAULT TRUE
                                );";

        $createVendorRates = "CREATE TABLE IF NOT EXISTS vendor_rates
                                (
                                    time_id   bigint NOT NULL,
                                    vendor_id int    NOT NULL REFERENCES vendor_list (id) ON UPDATE CASCADE ON DELETE CASCADE,
                                    rates     JSONB  NOT NULL,
                                    PRIMARY KEY (time_id, vendor_id)
                                );";

        $createVendorDataLog = "CREATE TABLE IF NOT EXISTS vendor_data_log
                                (
                                    time_id        bigint  NOT NULL,
                                    vendor_id      int     NOT NULL REFERENCES vendor_list (id) ON UPDATE CASCADE ON DELETE CASCADE,
                                    validate_error varchar NOT NULL,
                                    origin_data    varchar NOT NULL,
                                    PRIMARY KEY (time_id, vendor_id)
                                );";

        $this->addSql($createClientApi);
        $this->addSql($createVendorList);
        $this->addSql($createVendorRates);
        $this->addSql($createVendorDataLog);
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP TABLE IF EXISTS vendor_data_log;');
        $this->addSql('DROP TABLE IF EXISTS vendor_rates;');
        $this->addSql('DROP TABLE IF EXISTS vendor_list;');
        $this->addSql('DROP TABLE IF EXISTS client_api;');
    }
}
