<?php

namespace Paybis\ExchangeRates\App\Storages\PG\Entity;

use Paybis\ExchangeRates\App\Storages\PG\Repository\ClientApiRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * ClientApi
 *
 * @ORM\Table(name="client_api")
 * @ORM\Entity(repositoryClass=ClientApiRepository::class)
 */
class ClientApi
{
    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="client_api_email_seq", allocationSize=1, initialValue=1)
     */
    private $email;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="api_key", type="string", length=64, nullable=false, options={"fixed"=true})
     */
    private $apiKey;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", nullable=false)
     */
    private $name;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_active", type="boolean", nullable=false, options={"default"="1"})
     */
    private $isActive = true;

    /**
     * ClientApi constructor.
     * @param string $email
     * @param string $apiKey
     * @param string $name
     * @param bool $isActive
     */
    public function __construct(string $email, string $apiKey, string $name, bool $isActive = true)
    {
        $this->email = $email;
        $this->apiKey = $apiKey;
        $this->name = $name;
        $this->isActive = $isActive;
    }


    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getApiKey(): string
    {
        return $this->apiKey;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->isActive;
    }

    /**
     * @param string $apiKey
     */
    public function setApiKey(string $apiKey): void
    {
        $this->apiKey = $apiKey;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @param bool $isActive
     */
    public function setIsActive(bool $isActive): void
    {
        $this->isActive = $isActive;
    }
}
