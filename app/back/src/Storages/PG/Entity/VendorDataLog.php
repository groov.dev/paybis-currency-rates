<?php

namespace Paybis\ExchangeRates\App\Storages\PG\Entity;

use Paybis\ExchangeRates\App\Storages\PG\Repository\VendorDataLogRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * VendorDataLog
 *
 * @ORM\Table(name="vendor_data_log")
 * @ORM\Entity(repositoryClass=VendorDataLogRepository::class)
 */
class VendorDataLog
{
    /**
     * @var int
     *
     * @ORM\Column(name="time_id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $timeId;

    /**
     * @var string
     *
     * @ORM\Column(name="validate_error", type="string", nullable=false)
     */
    private $validateError;

    /**
     * @var string
     *
     * @ORM\Column(name="origin_data", type="string", nullable=false)
     */
    private $originData;

    /**
     * @var VendorList
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="VendorList")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="vendor_id", referencedColumnName="id")
     * })
     */
    private $vendor;

    /**
     * VendorDataLog constructor.
     * @param int $timeId
     * @param string $validateError
     * @param string $originData
     * @param VendorList $vendor
     */
    public function __construct(int $timeId, string $validateError, string $originData, VendorList $vendor)
    {
        $this->timeId = $timeId;
        $this->validateError = $validateError;
        $this->originData = $originData;
        $this->vendor = $vendor;
    }

    /**
     * @return int
     */
    public function getTimeId(): int
    {
        return $this->timeId;
    }

    /**
     * @return string
     */
    public function getValidateError(): string
    {
        return $this->validateError;
    }

    /**
     * @return string
     */
    public function getOriginData(): string
    {
        return $this->originData;
    }

    /**
     * @return VendorList
     */
    public function getVendor(): VendorList
    {
        return $this->vendor;
    }
}
