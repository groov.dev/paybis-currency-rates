<?php

namespace Paybis\ExchangeRates\App\Storages\PG\Entity;

use Paybis\ExchangeRates\App\Storages\PG\Repository\VendorRatesRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * VendorRates
 *
 * @ORM\Table(name="vendor_rates")
 * @ORM\Entity(repositoryClass=VendorRatesRepository::class)
 *
 */
class VendorRates
{
    /**
     * @var int
     *
     * @ORM\Column(name="time_id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $timeId;

    /**
     * @var string
     *
     * @ORM\Column(name="rates", type="string", nullable=false)
     */
    private $rates;

    /**
     * @var VendorList
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="VendorList")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="vendor_id", referencedColumnName="id")
     * })
     */
    private $vendor;

    /**
     * VendorRates constructor.
     * @param int $timeId
     * @param string $rates
     * @param VendorList $vendor
     */
    public function __construct(int $timeId, string $rates, VendorList $vendor)
    {
        $this->timeId = $timeId;
        $this->rates = $rates;
        $this->vendor = $vendor;
    }


    /**
     * @return int
     */
    public function getTimeId(): int
    {
        return $this->timeId;
    }

    /**
     * @param int $timeId
     */
    public function setTimeId(int $timeId): void
    {
        $this->timeId = $timeId;
    }

    /**
     * @return string
     */
    public function getRates(): string
    {
        return $this->rates;
    }

    /**
     * @param string $rates
     */
    public function setRates(string $rates): void
    {
        $this->rates = $rates;
    }

    /**
     * @return VendorList
     */
    public function getVendor(): VendorList
    {
        return $this->vendor;
    }

    /**
     * @param VendorList $vendor
     */
    public function setVendor(VendorList $vendor): void
    {
        $this->vendor = $vendor;
    }
}
