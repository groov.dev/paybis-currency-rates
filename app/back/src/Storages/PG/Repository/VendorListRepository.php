<?php

namespace Paybis\ExchangeRates\App\Storages\PG\Repository;

use Paybis\ExchangeRates\App\Storages\PG\Entity\VendorList;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method VendorList|null find($id, $lockMode = null, $lockVersion = null)
 * @method VendorList|null findOneBy(array $criteria, array $orderBy = null)
 * @method VendorList[]    findAll()
 * @method VendorList[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VendorListRepository extends ServiceEntityRepository
{
    /**
     * VendorListRepository constructor.
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, VendorList::class);
    }

    /**
     * @param string $vendorKey
     * @return VendorList|null
     */
    public function getVendorByKey(string $vendorKey): ?VendorList
    {
        return $this->findOneBy(["key" => $vendorKey]) ?: null;
    }

    /**
     * @param bool $active
     * @return VendorList[]|null
     */
    public function getVendorsByActive(bool $active = true): array
    {
        return
            $this
                ->createQueryBuilder("v")
                ->where("v.isActive=:active")
                ->setParameter("active", $active)
                ->getQuery()
                ->getResult() ?: [];
    }
}
