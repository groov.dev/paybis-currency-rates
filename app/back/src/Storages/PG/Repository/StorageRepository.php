<?php


namespace Paybis\ExchangeRates\App\Storages\PG\Repository;


use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Statement;
use Doctrine\DBAL\Driver\Exception as DriverException;
use Doctrine\DBAL\Exception as DBALException;


/**
 * Class StorageRepository
 * @package Paybis\ExchangeRates\App\Storages\PG\Repository
 */
abstract class StorageRepository extends ServiceEntityRepository
{
    /** @var ManagerRegistry */
    protected $registry;

    /** @var string */
    protected $className;

    /**
     * StorageRepository constructor.
     * @param ManagerRegistry $registry
     * @param string $className
     */
    public function __construct(ManagerRegistry $registry, string $className)
    {
        $this->registry = $registry;
        $this->className = $className;
        parent::__construct($registry, $className);
    }


    /**
     * @return string
     */
    public function getTableName(): string
    {
        return $this->_em->getClassMetadata($this->className)->getTableName();
    }

    /**
     * @param string $sql
     * @param int|null $fetchMode
     * @return array
     * @throws DBALException
     * @throws DriverException
     */
    protected function fetchAll(string $sql, int $fetchMode = null): array
    {
        $stmt = $this->execSql($sql);

        return $stmt->fetchAll($fetchMode);
    }

    /**
     * @param string $sql
     * @return Statement
     * @throws DriverException
     * @throws DBALException
     */
    protected function execSql(string $sql): Statement
    {
        $conn = $this->getConnection();
        $stmt = $conn->prepare($sql);
        $stmt->executeQuery();

        return $stmt;
    }

    /**
     * @param string|null $name
     * @return Connection
     */
    protected function getConnection(string $name = null): Connection
    {
        return $this->registry->getManager($name)->getConnection();
    }
}