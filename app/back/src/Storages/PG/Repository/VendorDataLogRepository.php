<?php

namespace Paybis\ExchangeRates\App\Storages\PG\Repository;

use Paybis\ExchangeRates\App\Storages\PG\Entity\VendorDataLog;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Paybis\ExchangeRates\App\Storages\PG\Entity\VendorList;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\OptimisticLockException;


/**
 * @method VendorDataLog|null find($id, $lockMode = null, $lockVersion = null)
 * @method VendorDataLog|null findOneBy(array $criteria, array $orderBy = null)
 * @method VendorDataLog[]    findAll()
 * @method VendorDataLog[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VendorDataLogRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, VendorDataLog::class);
    }

    /**
     * @param VendorList $vendor
     * @param string $originData
     * @param string $validateError
     * @param int $timeId
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function logErrorVendorRatesOutputData(VendorList $vendor, string $originData, string $validateError, int $timeId): void
    {
        if (!$originData || !$validateError) return;

        $row = $this->findOneBy(["timeId" => $timeId, "vendor" => $vendor]);

        if (is_object($row)) return;

        $row = new VendorDataLog($timeId, $validateError, $originData, $vendor);

        $this->_em->persist($row);
        $this->_em->flush();
    }
}
