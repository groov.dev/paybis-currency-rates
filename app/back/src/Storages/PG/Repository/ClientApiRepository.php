<?php

namespace Paybis\ExchangeRates\App\Storages\PG\Repository;

use Paybis\ExchangeRates\App\Storages\PG\Entity\ClientApi;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ClientApi|null find($id, $lockMode = null, $lockVersion = null)
 * @method ClientApi|null findOneBy(array $criteria, array $orderBy = null)
 * @method ClientApi[]    findAll()
 * @method ClientApi[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ClientApiRepository extends ServiceEntityRepository
{
    /**
     * ClientApiRepository constructor.
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ClientApi::class);
    }

    /**
     * @param string $apiKey
     * @return bool
     */
    public function isValidApiKey(string $apiKey): bool
    {
        $client = $this->getClientApiByKey($apiKey);

        if (null === $client) return false;

        return $client->isActive();
    }

    /**
     * @param string $apiKey
     * @return ClientApi|null
     */
    public function getClientApiByKey(string $apiKey): ?ClientApi
    {
        return $this->findOneBy(["apiKey" => $apiKey]) ?: null;
    }
}
