<?php

namespace Paybis\ExchangeRates\App\Storages\PG\Repository;

use Paybis\ExchangeRates\App\Helper\QueryHelper;
use Paybis\ExchangeRates\App\Module\ClientApi\Request\IClientApiRequest;
use Paybis\ExchangeRates\App\Storages\PG\Entity\VendorRates;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\DBAL\Driver\Exception as DriverException;
use Doctrine\DBAL\Exception as DBALException;


/**
 * @method VendorRates|null find($id, $lockMode = null, $lockVersion = null)
 * @method VendorRates|null findOneBy(array $criteria, array $orderBy = null)
 * @method VendorRates[]    findAll()
 * @method VendorRates[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VendorRatesRepository extends StorageRepository
{
    /** @var string */
    private $groupField = "vendor_id";

    /** @var string */
    private $constrField = "time_id";


    /**
     * VendorRatesRepository constructor.
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, VendorRates::class);
    }

    /**
     * @param IClientApiRequest $requestData
     * @return array
     * @throws DBALException
     * @throws DriverException
     */
    public function getCurrencyRates(IClientApiRequest $requestData): array
    {
        $sql = $this->getQueryRatesO($requestData);
        $arr = $this->fetchAll($sql, \PDO::FETCH_GROUP);

        $v = array_flip($requestData->getVendors());

        $d = [];

        foreach ($arr as $id => $data) {
            $d[$v[$id]] = $data;
        }

        return $d;
    }

    /**
     * @param IClientApiRequest $r
     * @return string
     */
    private function getQueryRatesO(IClientApiRequest $r): string
    {
        $rates = $this->getSelectRates($r->getCurrencyBase(), $r->getCurrenciesRates());
        $rates = sprintf("%s, %s, %s", $this->groupField, $this->constrField, $rates);

        return vsprintf(QueryHelper::getRatesQueryPattern(), [$rates, $this->getTableName(), $this->constraint($r->getVendors()), $r->getStart(), $r->getEnd()]);
    }

    /**
     * @param string $base
     * @param array $bases
     * @return string
     */
    private function getSelectRates(string $base, array $bases): string
    {
        $sql = [];
        unset($bases[$base]);
        foreach ($bases as $curr) {
            $sql[] = vsprintf(QueryHelper::getCurrencyRatesPattern(), [$curr, $base, $curr]);
        }

        return implode(",", $sql);
    }

    /**
     * @param array $vendors
     * @return string
     */
    private function constraint(array $vendors): string
    {
        if ($vendors[QueryHelper::ALL] ?? false) return $this->constrField;

        return sprintf("%s in(%s) and %s", $this->groupField, implode(",", array_values($vendors)), $this->constrField);
    }
}
