<?php
declare(strict_types=1);

namespace Paybis\ExchangeRates\App\Storages\Redis;


use Paybis\ExchangeRates\App\Helper\DateHelper;
use Paybis\ExchangeRates\App\Settings\IRedisSettings;
use Redis;


/**
 * Class PaybisRedis
 * @package Paybis\ExchangeRates\App\Storages\Redis
 */
class PaybisRedis extends Redis implements IRedisSettings
{
    /**
     * max value for range
     */
    const RANGE_MAX = "+inf";

    /**
     * min value for range
     */
    const RANGE_MIN = "-inf";

    /**
     * Count for zLog()
     */
    const COUNT_LOG = 500;

    /**
     * TTL for simple log
     */
    const TTL_LOG = 300;

    /**
     * Main key identity is a API Value
     */
    const KEY_API = "key_api";

    /**
     * Identity valid
     */
    const VALID = "1";

    /**
     * Identity invalid
     */
    const INVALID = "-1";

    /**
     * Key for value rate_limit
     */
    const KEY_RATE_LIMIT = "rate_limit";

    /**
     * Key for daemon sleep
     */
    const KEY_DAEMON_SLEEP = "daemon:sleep";

    /**
     *  turn on/off daemon. Default on
     */
    const KEY_DAEMON_TURNOFF = "daemon:turnoff";

    /**
     * Key with TTL indicate what daemon is alive
     */
    const KEY_IS_ALIVE_DAEMON = "is_alive:daemon_key";

    /**
     * Key with TTL indicate frequency request
     */
    const KEY_TIME_REQUEST = "time_request";

    const KEY_LOG_ERROR_INPUT = "log_error_input";


    /**
     * Default time request  for client
     */
    const SECOND_TIME_REQUEST = 2;

    /**
     * Time
     */
    const SECOND_RATE_LIMIT = 5 * DateHelper::SEC_MIN;

    /**
     * Seconds for daemon sleep
     */
    const SECOND_DAEMON_SLEEP = 25 * DateHelper::SEC_MIN;


    /**
     * @param string $key
     * @return bool
     */
    public function isEmptyApiKey(string $key): bool
    {
        $apiData = $this->get($key);

        return empty($apiData);
    }

    /**
     * @param string $key
     */
    public function incRateLimitCount(string $key): void
    {
        $key = $this->getRateLimitKey($key);
        $count = (int)$this->get($key);
        $count++;
        $this->set($key, $count, self::SECOND_RATE_LIMIT); //Its need for set TTL
    }

    /**
     * @param string $apiKey
     * @param int|null $ttl
     * @return bool
     */
    public function isTimeRequestLimit(string $apiKey, int $ttl = null): bool
    {
        $redisKey = self::toKey([self::KEY_TIME_REQUEST, $apiKey]);

        if (!$this->isEmptyApiKey($redisKey)) return true;

        $this->set($redisKey, self::VALID, self::SECOND_TIME_REQUEST);

        return false;
    }

    /**
     * @param string $vendorKey
     * @param string $errors
     */
    public function logErrorVendorRatesInputData(string $vendorKey, string $errors)
    {
        if (!$errors) return;

        $key = self::KEY_LOG_ERROR_INPUT . ":" . $vendorKey;

        $this->set($key, $errors, self::TTL_LOG);
    }


    /**
     * @param $key
     * @param string $message
     */
    public function pushLog($key, string $message): void
    {
        $this->zAdd($key, [], time(), $message);
        $this->cutLog($key);
    }

    public function getLog(string $key, int $count, int $offset = 0, bool $scores = false): array
    {
        $options = ['withscores' => $scores, 'limit' => [$offset, $count]];

        $start = time();
        $end = self::RANGE_MIN;

        return $this->zRevRangeByScore($key, $start, $end, $options) ?: [];
    }

    /**
     * @param $key
     */
    public function cutLog($key): void
    {
        $count = $this->zCount($key, self::RANGE_MIN, self::RANGE_MAX);

        if (self::COUNT_LOG > $count) return;

        $count = 1;
        $offset = 0;
        $scores = true;

        $val = $this->getLog($key, $count, $offset, $scores);
        if (empty($val)) return;
        $end = reset($val);

        $this->zRemRangeByScore($key, self::RANGE_MIN, $end);
    }


    /**
     * @param string $apiKey
     * @return bool|null
     */
    public function isValidClientKey(string $apiKey): ?bool
    {
        $redisKey = self::getApiKey($apiKey);

        if ($this->isEmptyApiKey($redisKey)) return null;

        return self::VALID == $this->get($redisKey);
    }

    /**
     * @param string $key
     * @param string $val
     * @param int|null $ttl
     */
    public function setWithApiKey(string $key, string $val, int $ttl = null)
    {
        if (!$key) return;

        $redisKey = self::getApiKey($key);

        $this->set($redisKey, $val, $ttl);
    }

    /**
     * @param string $key
     * @return string|null
     */
    public function getWithApiKey(string $key): ?string
    {
        if (!$key) return null;

        $redisKey = self::getApiKey($key);

        $val = $this->get($redisKey);

        return $val ?: null;
    }


    /**
     * @return void
     */
    public function daemonIsAliveUpdate(): void
    {
        $this->set(self::KEY_IS_ALIVE_DAEMON, self::VALID, 2 * $this->daemonSleep());
    }

    /**
     * @return bool
     */
    public function daemonIsAlive(): bool
    {
        return self::VALID == $this->get(self::KEY_IS_ALIVE_DAEMON);
    }

    /**
     * @return int
     */
    public function daemonSleep(): int
    {
        $sleep = (int)$this->get(self::KEY_DAEMON_SLEEP);
        if ($sleep < 60) {
            $sleep = self::SECOND_DAEMON_SLEEP;
            $this->set(self::KEY_DAEMON_SLEEP, $sleep);
        }

        return $sleep;
    }

    /**
     * @return bool
     */
    public function daemonIsTurnOff(): bool
    {
        return self::VALID == $this->get(self::KEY_DAEMON_TURNOFF);
    }

    /**
     * @param string $key
     * @return string
     */
    public static function getRateLimitKey(string $key): string
    {
        return self::toKey([self::KEY_RATE_LIMIT, $key]);
    }

    /**
     * @param string $key
     * @return string
     */
    public static function getApiKey(string $key): string
    {
        return self::toKey([self::KEY_API, $key]);
    }

    /**
     * @param array $arr
     * @param string $separator
     * @return string
     */
    public static function toKey(array $arr, string $separator = ":"): string
    {
        if (empty($arr)) return "";

        return implode($separator, $arr);
    }
}