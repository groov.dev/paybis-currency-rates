<?php

namespace Paybis\ExchangeRates\App\Subscriber;


use Paybis\ExchangeRates\App\Module\ClientApi\Request\ClientApiRequestError;
use Paybis\ExchangeRates\App\Module\ClientApi\Response\ErrorResponse;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Class ExceptionSubscriber
 * @package Paybis\ExchangeRates\App\Subscriber
 */
class ExceptionSubscriber implements EventSubscriberInterface
{
    /**
     * @return \array[][]
     */
    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::RESPONSE => [
                ['checkResponseNotFound', 0],
            ],
        ];
    }

    /**
     * @param ResponseEvent $event
     */
    public function checkResponseNotFound(ResponseEvent $event)
    {
        if (!$event->isMainRequest()) return;

        $code = $event->getResponse()->getStatusCode();

        if (Response::HTTP_NOT_FOUND != $code) return;

        $err = ErrorResponse::create(Response::HTTP_NOT_FOUND, ClientApiRequestError::MESS_NOT_FOUND, ErrorResponse::DESCRIPTION_NOT_FOUNT);
        $event->setResponse($err->getResponse());
    }
}