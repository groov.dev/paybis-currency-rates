<?php
declare(strict_types=1);

namespace Paybis\ExchangeRates\App\Factory;

use Paybis\ExchangeRates\App\Storages\Redis\PaybisRedis;

class RedisFactory
{
    public static function createRedis(string $host, int $port, string $password = null): PaybisRedis
    {
        $redis = new PaybisRedis();

        if (!$redis->connect($host, $port)) {
            $mes = sprintf("Can't connect to Redis server host:%s, port:%s", $host, $port);
            throw new \RuntimeException($mes);
        }

        if (is_null($password)) {
            return $redis;
        }

        if (!$redis->auth($password)) {
            $mes = sprintf("Can't AUTH to Redis server password:%s", $password);
            throw new \RuntimeException($mes);
        }

        return $redis;
    }
}