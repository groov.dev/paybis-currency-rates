<?php

declare(strict_types=1);

namespace Paybis\ExchangeRates\App\Controller;

use Paybis\ExchangeRates\App\Module\ClientApi\Request\ClientApiRequest;
use Paybis\ExchangeRates\App\Module\ClientApi\Response\DataResponse;
use Paybis\ExchangeRates\App\Module\ClientApi\Response\ErrorResponse;
use Paybis\ExchangeRates\App\Module\ClientApi\Response\InfoResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Paybis\ExchangeRates\App\Settings\Settings;
use Doctrine\DBAL\Driver\Exception as DriverException;
use Doctrine\DBAL\Exception as DBALException;


/**
 * Class ApiController
 * @package Paybis\ExchangeRates\App\Controller
 */
class ApiController extends AbstractController
{
    /**
     * Show information how to use API
     *
     * @Route(
     *     "/api/info",
     *     methods={"GET"},
     *     name="api-info"
     * )
     *
     * @return JsonResponse
     */
    public function getInfo(): JsonResponse
    {
        //@NOTE 1) Create prepare response
        $response = InfoResponse::create();

        //@NOTE 2) Return JsonResponse
        return $response->getJsonResponse();
    }

    /**
     * Main entrypoint
     *
     * @Route(
     *     "/api",
     *     methods={"GET"},
     *     name="api-currency-cates"
     * )
     *
     * @param Settings $settings
     * @param ClientApiRequest $request
     * @return JsonResponse
     * @throws DriverException
     * @throws DBALException
     */
    public function getCurrencyRates(Settings $settings, ClientApiRequest $request): JsonResponse
    {
        //@NOTE 1) Check if errors exists
        if ($request->getError()) {
            return ErrorResponse::createRequest($request)->getJsonResponse();
        }

        //@NOTE 2) Get array currency rates
        $data = $settings->getCurrencyRates($request);

        //@NOTE 3) Create prepare response
        $response = DataResponse::create($request, $data);

        //@NOTE 4) Return JsonResponse
        return $response->getJsonResponse();
    }
}