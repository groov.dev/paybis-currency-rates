<?php
declare(strict_types=1);

namespace Paybis\ExchangeRates\App\Helper;

class DateHelper
{
    const SEC_MIN = 60;
    const SEC_HOUR = 3600;
    const SEC_DAY = 86400;

    const FORM_HOAR = "Y-m-d H:00:00";

    const FORM_MY_SQL = "Y-m-d H:i:s";

    /**
     * @return int
     */
    public static function getHourTs(): int
    {
        return strtotime(date(self::FORM_HOAR));
    }

    /**
     * @param int|null $time
     * @param string $format
     * @return string
     */
    public static function timeToStr(int $time = null, string $format = self::FORM_MY_SQL): string
    {
        return date($format, $time);
    }

    /**
     * @param null $time
     * @return int
     */
    public static function getTimeStart($time = null): int
    {
        $lastDay = time() - self::SEC_DAY;

        if (!$time) return $lastDay;

        $time = intval($time);

        return $time < 1 ? time() - self::SEC_DAY : $time;
    }

    /**
     * @param null $time
     * @return int
     */
    public static function getTimeEnd($time = null): int
    {
        $now = time();

        if (!$time) return $now;

        $time = intval($time);

        return $time < 1 ? $now : $time;
    }

    /**
     * @param $timeStart
     * @param $timeEnd
     * @return array
     */
    public static function getTimeRange($timeStart, $timeEnd): array
    {
        $timeStart = self::getTimeStart($timeStart);
        $timeEnd = self::getTimeEnd($timeEnd);

        if ($timeStart >= $timeEnd) {
            $timeStart = self::getTimeStart();

            $timeEnd = self::getTimeEnd();
        }

        return [$timeStart, $timeEnd];
    }

    /**
     * @return int
     */
    public static function tsSubDay(): int
    {
        return time() - self::SEC_DAY;
    }

    /**
     * @return int
     */
    public static function tsYesterday(): int
    {
        return time() - self::SEC_DAY;
    }

    /**
     * @return int
     */
    public static function tsSubHour(): int
    {
        return time() - self::SEC_HOUR;
    }

}