<?php


namespace Paybis\ExchangeRates\App\Helper;


use RecursiveArrayIterator;
use RecursiveIteratorIterator;


/**
 * Class AdapterHelper
 * @package Paybis\ExchangeRates\App\Helper
 */
class AdapterHelper
{
    /**
     * Rate for base currency
     */
    const BASE_RATE = 1;

    /**
     * @param string $base
     * @param array $rates
     * @return array
     */
    public static function getAdapterArray(string $base, array $rates): array
    {
        return [
            "base" => $base,
            "rates" => $rates
        ];
    }

    /**
     * @param string $base
     * @param array $rates
     * @return string
     */
    public static function getAdapterJson(string $base, array $rates): string
    {
        return json_encode(self::getAdapterArray($base, self::setBase($base, $rates)));
    }


    /**
     * @param string $key
     * @param float $value
     * @return float[]
     */
    public static function getRatesRow(string $key, float $value): array
    {
        return [$key => $value];
    }


    public static function arrayConfigFormat(array $arr): array
    {
        $arr = self::arrayFlatten($arr);
        $arr = self::arrayUpperVal($arr);
        $arr = self::arrayKeyVal($arr);

        return $arr;
    }

    /**
     * @param array $arr
     * @return array
     */
    public static function arrayKeyVal(array $arr): array
    {
        if (empty($arr)) return [];

        return array_combine($arr, $arr);
    }

    /**
     * @param array $arr
     * @return array
     */
    public static function arrayUpperVal(array $arr): array
    {
        if (empty($arr)) return [];

        array_walk($arr, function (&$val) {
            if (is_string($val)) $val = strtoupper($val);
        });

        return $arr;
    }


    /**
     * @param array $arr
     * @return array
     */
    public static function arrayFlatten(array $arr): array
    {
        $flat = [];
        $it = new RecursiveIteratorIterator(new RecursiveArrayIterator($arr));
        foreach ($it as $v) {
            $flat[] = $v;
        }

        return $flat;
    }

    /**
     * @param string $currency
     * @return bool
     */
    public static function isValidCurrency(string $currency): bool
    {
        if (strlen($currency) != 3) return false;

        return ctype_alpha($currency);
    }

    /**
     * @param string $base
     * @param array $rates
     * @return array
     */
    public static function setBase(string $base, array $rates): array
    {
        $val = (int)($rates[$base] ?? 0);

        if (self::BASE_RATE == $val || !$val) {
            $rates[$base] = self::BASE_RATE;
            return $rates;
        }

        return $rates;
    }
}