<?php


namespace Paybis\ExchangeRates\App\Helper;


/**
 * Class QueryHelper
 * @package Paybis\ExchangeRates\App\Helper
 */
class QueryHelper
{
    const ALL = 'all';

    /**
     * @return string
     */
    public static function getSelectPattern(): string
    {
        return 'select %s from %s';
    }

    /**
     * @return string
     */
    public static function getWherePattern(): string
    {
        return "where %s";
    }

    /**
     * @return string
     */
    public static function getBetweenIntPattern(): string
    {
        return "between %d and %d";
    }

    /**
     * @return string
     */
    public static function getBetweenStrPattern(): string
    {
        return "between '%s' and '%s'";
    }

    /**
     * @return string
     */
    public static function getOrderByPattern(): string
    {
        return "order by %s %s";
    }

    /**
     * @return string
     */
    public static function getLimitPattern(): string
    {
        return "limit %d offset %d";
    }

    /**
     * @return string
     */
    public static function getCurrencyRatesPattern(): string
    {
        return "((rates->'rates'->'%s')::FLOAT/(rates->'rates'->'%s')::FLOAT) as %s";
    }


    /**
     * @return string
     */
    public static function getRatesQueryPattern(): string
    {
        return implode(' ', [self::getSelectPattern(), self::getWherePattern(), self::getBetweenIntPattern()]);
    }
}