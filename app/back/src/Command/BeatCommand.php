<?php
declare(strict_types=1);

namespace Paybis\ExchangeRates\App\Command;

use Paybis\ExchangeRates\App\Command\VendorRates\VendorRatesCommand;
use Paybis\ExchangeRates\App\Settings\Settings;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Process\Process;
use Throwable;

/**
 * Class BeatCommand
 * @package Paybis\ExchangeRates\App\Command
 */
class BeatCommand extends Command
{
    /**
     * @var string
     */
    public static $defaultName = 'daemon:vendor-update';

    /** @var ParameterBagInterface $bag */
    protected $bag;

    /** @var Settings $settings */
    protected $settings;

    /** @var array */
    private $runningProcesses = [];

    protected function configure()
    {
        $this
            ->setDescription('Start daemon with infinity loop for update data from Vendor API')
            ->setHelp('This command get last data from vendor API and insert to local store');
    }

    /**
     * CronCommand constructor.
     * @param ParameterBagInterface $bag
     * @param Settings $settings
     */
    public function __construct(ParameterBagInterface $bag, Settings $settings)
    {
        parent::__construct();
        $this->bag = $bag;
        $this->settings = $settings;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->work();
        return Command::SUCCESS;
    }

    /**
     * Main process
     */
    private function work()
    {
        while (1) {
            $this->process();
            $this->sleep();
        }
    }

    /**
     *  Main process
     */
    private function process()
    {
        if ($this->isTurnOff()) return;

        $this->runProcesses();
        $this->waitProcesses();
    }

    /**
     * Sleep
     */
    private function sleep(): void
    {
        $this->settings->daemonIsAliveUpdate();
        sleep($this->settings->daemonSleep());
    }

    /**
     * @return bool
     */
    private function isTurnOff(): bool
    {
        return $this->settings->daemonIsTurnOff();
    }

    /**
     * }
     * @return array
     */
    public function getVendors(): array
    {
        $vend = [];
        foreach ($this->settings->getActiveVendorsKey() as $key => $vendor) {
            $cmd = VendorRatesCommand::CLI_KEY . $key;
            try {
                $this->getApplication()->find($cmd);
                $vend[] = $cmd;
            } catch (Throwable $e) {
                //@TODO Log
                printf("Command '%s' not found\n ", $cmd);
            }
        }

        return $vend;
    }

    /**
     * Process wait while all commands stop work
     */
    private function waitProcesses()
    {
        while (count($this->runningProcesses)) {

            /**
             * @var int $i
             * @var Process $process
             */
            foreach ($this->runningProcesses as $i => $process) {

                $process->checkTimeout();

                try {
                    $process->checkTimeout();
                } catch (Throwable $e) {
                    unset($this->runningProcesses[$i]);
                    $mes = vsprintf("Process out: '%s' , Error message: %s, file: '%s' line: '%s'", [
                            $process->getOutput(),
                            $e->getMessage(),
                            $e->getFile(),
                            $e->getLine()]
                    );
                    $this->logOutput($mes, 'ERR');
                    continue;
                }

                if (!$process->isRunning()) {
                    $this->logOutput($process->getOutput(), 'END');
                    unset($this->runningProcesses[$i]);
                }
            }
            sleep(1);
        }
    }

    /**
     * @param int $timeout
     */
    private function runProcesses(int $timeout = 60)
    {
        foreach ($this->getVendors() as $vendor) {
            $process = new Process([$this->getBinConsolePath(), $vendor]);
            $process->setTimeout($timeout);
            $process->start();
            $this->runningProcesses[$process->getPid()] = $process;
        }
    }

    /**
     * @return string
     */
    protected function getBinConsolePath(): string
    {
        return $this->bag->get('kernel.project_dir') . '/bin/console';
    }

    /**
     * @TODO Log a process
     *
     * @param string $buff
     * @param string $type
     */
    protected function logOutput(string $buff, string $type = '')
    {
        //@TODO Log process
        print_r([$type => $buff]);
    }
}