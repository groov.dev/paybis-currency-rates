<?php
declare(strict_types=1);

namespace Paybis\ExchangeRates\App\Command\VendorRates;

use Doctrine\Persistence\ManagerRegistry as Registry;
use Paybis\ExchangeRates\App\Module\VendorRates\Opex\OpexVendorRates;

class OpexCommand extends VendorRatesCommand
{
    public static $defaultName = parent::CLI_KEY . 'opex';

    public function __construct(Registry $registry, OpexVendorRates $vendorRates)
    {
        parent::__construct($registry, $vendorRates);
    }

}