<?php


namespace Paybis\ExchangeRates\App\Command\VendorRates;


use Doctrine\Persistence\ManagerRegistry as Registry;
use Paybis\ExchangeRates\App\Helper\DateHelper;

use Paybis\ExchangeRates\App\Module\VendorRates\IVendorRates;
use Paybis\ExchangeRates\App\Storages\PG\Entity\VendorList;
use Paybis\ExchangeRates\App\Storages\PG\Entity\VendorRates;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

abstract class VendorRatesCommand extends Command
{
    const CLI_KEY = 'vendor-rates:';

    /** @var IVendorRates */
    private $vendorRates;

    /** @var Registry */
    private $registry;

    /**
     * VendorCommand constructor.
     * @param Registry $registry
     * @param IVendorRates $vendorRates
     */
    public function __construct(Registry $registry, IVendorRates $vendorRates)
    {
        parent::__construct();
        $this->registry = $registry;
        $this->vendorRates = $vendorRates;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $vendor = $this->getVendor();
        $timeId = DateHelper::getHourTs();

        if ($this->isDataExists($vendor, $timeId)) return Command::SUCCESS;

        //1) Get data
        $data = $this->getData();

        //Validate format data
        $this->validateData($data);

        //2) Store data
        $this->storeData($vendor, $timeId, $data);

        return Command::SUCCESS;
    }

    private function validateData(string $data)
    {
        //Implement into decorator(validator)
    }

    private function getData(): string
    {
        return $this->vendorRates->getAdapterJson();
    }

    /**
     * @param VendorList $vendor
     * @param int $timeId
     * @param string $data
     */
    private function storeData(VendorList $vendor, int $timeId, string $data)
    {
        $entity = new VendorRates($timeId, $data, $vendor);
        $em = $this->registry->getManager();
        $em->persist($entity);
        $em->flush();

        printf("Save new time %s \n", date(DateHelper::FORM_MY_SQL, $timeId));
    }


    /**
     * @param string $vendorKey
     * @return VendorList|null
     */
    private function getVendorList(string $vendorKey): ?VendorList
    {
        return $this->registry->getRepository(VendorList::class)->findOneBy(["key" => $vendorKey]);
    }

    /**
     * @return VendorList
     */
    private function getVendor(): VendorList
    {
        $vendorKey = $this->vendorRates->getVendorKey();
        $vendor = $this->getVendorList($vendorKey);

        if (!is_object($vendor)) {
            $mes = sprintf("Unknown vendor for VendorKey: %s", $vendorKey);
            throw new \RuntimeException($mes);
        }

        return $vendor;
    }

    /**
     * @param VendorList $vendor
     * @param int $timeId
     * @return bool
     */
    private function isDataExists(VendorList $vendor, int $timeId): bool
    {
        $exist = false;
        $row = $this->registry->getRepository(VendorRates::class)->findOneBy(["timeId" => $timeId, "vendor" => $vendor]);
        if ($row) {
            $mes = sprintf("Vendor: '%s', Time %s row exists. Return\n", $vendor->getName(),  date(DateHelper::FORM_MY_SQL, $row->getTimeId()));
            print($mes);
            $exist = true;
        }

        return $exist;
    }
}