<?php
declare(strict_types=1);
namespace Paybis\ExchangeRates\App\Command\VendorRates;


use Doctrine\Persistence\ManagerRegistry as Registry;

use Paybis\ExchangeRates\App\Module\VendorRates\Bitpay\BitpayVendorRates;

class BitpayCommand extends VendorRatesCommand
{
    public static $defaultName = parent::CLI_KEY . 'bitpay';

    public function __construct(Registry $registry, BitpayVendorRates $vendorRates)
    {
        parent::__construct($registry, $vendorRates);
    }


}