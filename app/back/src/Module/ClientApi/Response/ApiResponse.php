<?php


namespace Paybis\ExchangeRates\App\Module\ClientApi\Response;



use Paybis\ExchangeRates\App\Module\ClientApi\Request\IClientApiRequest;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Throwable;

/**
 * Class ApiResponse
 * @package Paybis\ExchangeRates\App\Module\Api\Response
 */
abstract class ApiResponse implements IApiResponse
{
    /** @var IClientApiRequest */
    private $apiRequest;

    /** @var Throwable */
    private $throwable;

    /**
     * @return IClientApiRequest|null
     */
    public function getApiRequest(): ?IClientApiRequest
    {
        return $this->apiRequest;
    }

    /**
     * @param IClientApiRequest $apiRequest
     */
    public function setApiRequest(IClientApiRequest $apiRequest): void
    {
        $this->apiRequest = $apiRequest;
    }

    /**
     * @return Throwable
     */
    public function getThrowable(): Throwable
    {
        return $this->throwable;
    }

    /**
     * @param Throwable $e
     */
    public function setThrowable(Throwable $e): void
    {
        $this->throwable = $e;
    }

    /**
     * @return Response
     */
    public function getResponse(): Response
    {
        return new Response(json_encode($this->toArray()), $this->getStatus());
    }

    /**
     * @return JsonResponse
     */
    public function getJsonResponse(): JsonResponse
    {
        return new JsonResponse($this->toArray(), $this->getStatus());
    }
}