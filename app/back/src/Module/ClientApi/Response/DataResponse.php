<?php

namespace Paybis\ExchangeRates\App\Module\ClientApi\Response;

use Paybis\ExchangeRates\App\Helper\DateHelper;
use Paybis\ExchangeRates\App\Module\ClientApi\Request\IClientApiRequest;
use Symfony\Component\HttpFoundation\Response;


/**
 * Class DataResponse
 * @package Paybis\ExchangeRates\App\Module\Api\Response
 */
class DataResponse extends ApiResponse
{
    /** @var string */
    private $base;

    /** @var string */
    private $start_time;

    /** @var string */
    private $end_time;

    /** @var array */
    private $rates;

    /**
     * @return bool
     */
    private function isLatest(): bool
    {
        return count($this->rates) == 1;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return get_object_vars($this);
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return Response::HTTP_OK;
    }


    /**
     * @return string
     */
    public function getBase(): string
    {
        return $this->base;
    }

    /**
     * @return string
     */
    public function getStartTime(): string
    {
        return $this->start_time;
    }

    /**
     * @return string
     */
    public function getEndTime(): string
    {
        return $this->end_time;
    }

    /**
     * @return array
     */
    public function getRates(): array
    {
        return $this->rates;
    }


    /**
     * @param string $base
     */
    private function setBase(string $base): void
    {
        $this->base = $base;
    }

    /**
     * @param int $start_time
     */
    private function setStartTime(int $start_time): void
    {
        if ($this->isLatest() && $start_time < 1) {
            $start_time = DateHelper::tsSubHour();
        }

        $this->start_time = date(DATE_ATOM, $start_time);
    }

    /**
     * @param int $end_time
     */
    private function setEndTime(int $end_time): void
    {
        if ($this->isLatest() && $end_time < 1) {
            $end_time = time();
        }

        $this->end_time = date(DATE_ATOM, $end_time);
    }

    /**
     * @param array $rates
     */
    private function setRates(array $rates): void
    {
        $this->rates = $rates;
    }

    /**
     * @param IClientApiRequest $request
     * @param array $data
     * @return DataResponse
     */
    public static function create(IClientApiRequest $request, array $data): DataResponse
    {
        $self = new self();
        $self->setApiRequest($request);
        $self->setRates($data);
        $self->setBase($request->getCurrencyBase());
        $self->setEndTime($request->getEnd());
        $self->setStartTime($request->getStart());

        return $self;
    }
}