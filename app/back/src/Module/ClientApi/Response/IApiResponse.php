<?php


namespace Paybis\ExchangeRates\App\Module\ClientApi\Response;


use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * Interface IApiResponse
 * @package Paybis\ExchangeRates\App\Module\Api\Response
 */
interface IApiResponse
{
    /**
     * @return Response
     */
    public function getResponse(): Response;

    /**
     * @return JsonResponse
     */
    public function getJsonResponse(): JsonResponse;

    /**
     * @return array
     */
    public function toArray(): array;

    /**
     * @return int
     */
    public function getStatus(): int;
}