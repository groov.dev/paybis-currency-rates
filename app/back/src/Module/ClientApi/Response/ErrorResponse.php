<?php

namespace Paybis\ExchangeRates\App\Module\ClientApi\Response;


use Paybis\ExchangeRates\App\Module\ClientApi\Request\IClientApiRequest;
use Paybis\ExchangeRates\App\Module\ClientApi\Request\IClientApiRequestError;
use Symfony\Component\HttpFoundation\Response;
use Throwable;


/**
 * Class ErrorResponse
 * @package Paybis\ExchangeRates\App\Module\Api\Response
 */
class ErrorResponse extends ApiResponse
{
    const DESCRIPTION_NOT_FOUNT = "Entry point not found. For more information please see: '/api/info' ";

    /**
     * @var bool
     */
    private $error = true;
    /**
     * @var int
     */
    private $status;

    /**
     * @var string
     */
    private $message;

    /**
     * @var string
     */
    private $description = "";


    /**
     * @return array
     */
    public function toArray(): array
    {
        return get_object_vars($this);
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @return bool
     */
    public function isError(): bool
    {
        return $this->error;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param int $status
     */
    private function setStatus(int $status): void
    {
        $this->status = $status;
    }

    /**
     * @param string $message
     */
    private function setMessage(string $message): void
    {
        $this->message = $message;
    }

    /**
     * @param string $description
     */
    private function setDescription(string $description = ""): void
    {
        $this->description = $description;
    }

    /**
     * @param int $status
     * @param string $message
     * @param string $description
     * @return ErrorResponse
     */
    public static function create(int $status, string $message, string $description = ""): ErrorResponse
    {
        $self = new self();
        $self->setStatus($status);
        $self->setMessage($message);
        $self->setDescription($description);
        return $self;
    }

    /**
     * @param Throwable $e
     * @return ErrorResponse
     */
    public static function createThrowable(Throwable $e): ErrorResponse
    {
        $self = new self();

        $self->setThrowable($e);
        $self->setStatus(Response::HTTP_INTERNAL_SERVER_ERROR);
        $self->setMessage($e->getMessage());

        return $self;
    }

    /**
     * @param IClientApiRequest $request
     * @return ErrorResponse|null
     */
    public static function createRequest(IClientApiRequest $request): ?ErrorResponse
    {
        /** @var IClientApiRequestError $err */
        if (!$err = $request->getError()) return null;

        $self = new self();
        $self->setApiRequest($request);
        $self->setStatus($err->getCode());
        $self->setMessage($err->getMessage());

        return $self;
    }
}