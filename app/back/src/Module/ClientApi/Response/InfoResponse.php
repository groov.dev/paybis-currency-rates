<?php

namespace Paybis\ExchangeRates\App\Module\ClientApi\Response;

use Symfony\Component\HttpFoundation\Response;


/**
 * Class DataResponse
 * @package Paybis\ExchangeRates\App\Module\Api\Response
 */
class InfoResponse extends ApiResponse
{
    public function toArray(): array
    {
        return self::getInfoDataArr();
    }

    public function getStatus(): int
    {
        return Response::HTTP_OK;
    }

    public static function create(): InfoResponse
    {
        return new self();
    }

    /**
     * @return array
     */
    private function getInfoDataArr(): array
    {
        return [
            "Query Params" => self::queryParams(),
            "Examples" => [
                'Latest' => [
                    'Description' => "Return one row with latest rates",
                    'URL' => self::exampleQueryLatest(),
                    'Response' => self::exampleResponseLatest(),
                ],
                'Period' => [
                    'Description' => "Return rows with rates by time range",
                    'URL' => self::exampleQueryPeriod(),
                    'Response' => self::exampleResponsePeriod(),
                ],
                'Error' => [
                    'Description' => "Example miss API Key",
                    'URL' => self::exampleQueryError(),
                    'Response' => self::exampleResponseError()
                ],
            ]
        ];
    }

    public static function exampleQueryError(): string
    {
        return "https://{URL}/api?base=EUR";
    }

    public static function exampleQueryLatest(): string
    {
        return "https://{URL}/api?key=YOUR_APP_ID";
    }

    public static function exampleResponseLatest(): array
    {
        return [
            "base" => "BTC",
            "group" => "hour",
            "start_time" => "2021-07-12T07:00:00+03:00",
            "end_time" => "2021-07-12T08:00:00+03:00",
            "rates" => [
                "1626033601" => [
                    [
                        "usd" => "33897.76386960059",
                        "gbp" => "24394.89864416063",
                        "eur" => "28548.96791308857",
                        "jpy" => "3734126.8212293964",
                        "chf" => "31021.36911644563",
                        "aud" => "45296.09035733701",
                        "cad" => "42241.18997964473"
                    ]
                ]
            ]
        ];
    }


    public static function exampleQueryPeriod(): string
    {
        return "https://{URL}/api?key={YOUR_APP_ID}&start=2021-07-10T20:00:00&end=2021-07-11T00:00:00&base=EUR&vendors=all&rates=btc,usd,jpy";
    }

    public static function exampleResponsePeriod(): array
    {
        return [
            "base" => "EUR",
            "group" => "hour",
            "start_time" => "2021-07-10T20:00:00+03:00",
            "end_time" => "2021-07-11T00:00:00+03:00",
            "rates" => [
                "1625936412" => [
                    [
                        "btc" => "0.000034896769933156285",
                        "usd" => "1.1876992365469308",
                        "jpy" => "130.7716244399998"
                    ]
                ],
                "1625940010" => [
                    [
                        "btc" => "0.000035526497569967364",
                        "usd" => "1.1876992365469308",
                        "jpy" => "130.7716244399998"
                    ]
                ],
                "1625943609" => [
                    [
                        "btc" => "0.000035507222399057447",
                        "usd" => "1.1876992365469308",
                        "jpy" => "130.7716244399998"
                    ]
                ],
                "1625947225" => [
                    [
                        "btc" => "0.00003551747461886732",
                        "usd" => "1.1876992365469308",
                        "jpy" => "130.7716244399998"
                    ]
                ]
            ]
        ];
    }

    public static function queryParams(): array
    {
        return [
            'key' => "string Required\n Your unique API ID",
            'base' => "string Optional\n Change base currency (3-letter code, default: BTC)",
            'rates' => "string Optional\n Limit results to specific currencies (comma-separated list of 3-letter codes)",
            'vendors' => "string Optional\n {all} - all vendors or vendor keys {bitpay,binfo, opex}  ",
            'start' => "yyyy-mm-dd Optional\n The time series start date in YYYY-MM-DD H:i:s format",
            'end' => "yyyy-mm-dd Optional\n The time series end date in YYYY-MM-DD H:i:sformat (see notes)",
        ];
    }

    public static function exampleResponseError(): array
    {
        return [
            "error" => true,
            "status" => 401,
            "message" => "Missing an API Key",
            "description" => ""
        ];
    }
}