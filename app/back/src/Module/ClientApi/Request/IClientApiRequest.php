<?php
declare(strict_types=1);

namespace Paybis\ExchangeRates\App\Module\ClientApi\Request;

use Symfony\Component\HttpFoundation\Request;

interface IClientApiRequest
{

    /**
     * @return string
     */
    public function getVendorApiKey(): string;

    /**
     * @return string
     */
    public function getCurrencyBase(): string;

    /**
     * @return array
     */
    public function getVendors(): array;

    /**
     * @return array
     */
    public function getCurrenciesRates(): array;

    /**
     * @return int
     */
    public function getStart(): int;

    /**
     * @return int
     */
    public function getEnd(): int;

    /**
     * @return Request
     */
    public function getRequest(): Request;

    /**
     * @return IClientApiRequestError|null
     */
    public function getError(): ?IClientApiRequestError;

    /**
     * @param IClientApiRequestError|null $error
     */
    public function setError(?IClientApiRequestError $error): void;
}