<?php

declare(strict_types=1);

namespace Paybis\ExchangeRates\App\Module\ClientApi\Request;

use Paybis\ExchangeRates\App\Helper\AdapterHelper;
use Paybis\ExchangeRates\App\Helper\DateHelper;
use Paybis\ExchangeRates\App\Helper\QueryHelper;
use Paybis\ExchangeRates\App\Settings\Settings;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class ClientApiRequest
 * @package Paybis\ExchangeRates\App\Module\ClientApi\Request
 */
class ClientApiRequest implements IClientApiRequest
{
    const GET_CLIENT_KEY_API = 'key';
    const GET_CURRENCY_BASE = 'base';
    const GET_VENDORS_RATE = 'vendors';
    const GET_CURRENCY_RATE = 'rates';
    const GET_DATE_START = 'start';
    const GET_DATE_END = 'end';

    /** @var Request */
    protected $request;

    /** @var string */
    protected $key;

    /** @var string */
    protected $base;

    /** @var array */
    protected $vendors;

    /** @var array */
    protected $rates;

    /** @var int */
    protected $start = 0;

    /** @var int */
    protected $end = 0;

    /** @var IClientApiRequestError */
    protected $errors = null;

    protected function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * @return Request
     */
    public function getRequest(): Request
    {
        return $this->request;
    }

    /**
     * @return string
     */
    public function getVendorApiKey(): string
    {
        return $this->key;
    }

    /**
     * @return string
     */
    public function getCurrencyBase(): string
    {
        return $this->base;
    }

    /**
     * @return array
     */
    public function getVendors(): array
    {
        return $this->vendors;
    }

    /**
     * @return array
     */
    public function getCurrenciesRates(): array
    {
        return $this->rates;
    }

    /**
     * @return int
     */
    public function getStart(): int
    {
        return $this->start;
    }

    /**
     * @return int
     */
    public function getEnd(): int
    {
        return $this->end;
    }


    /**
     * @param string $key
     */
    private function setKey(string $key): void
    {
        $this->key = $key;
    }

    /**
     * @param string $base
     */
    private function setBase(string $base): void
    {
        $this->base = $base;
    }

    /**
     * @param array $rates
     */
    private function setRates(array $rates): void
    {
        $this->rates = $rates;
    }

    /**
     *
     * @param Settings $settings
     * @param string $vendors
     * @return ClientApiRequest|null
     */
    private function setVendorsQuery(Settings $settings, string $vendors): ?ClientApiRequest
    {
        $vendors = str_replace(' ', '', $vendors);

        if (empty($vendors)) {
            $err = ClientApiRequestError::createError(ClientApiRequestError::ERROR_MISSING_VENDORS);
            $this->setError($err);
            return $this;
        }

        $vendorKeys = explode(",", strtolower($vendors));

        if (in_array(QueryHelper::ALL, $vendorKeys)) {
            $vendors = $settings->getAllVendorsByKey();
            if (empty($vendors)) {
                $err = ClientApiRequestError::createError(ClientApiRequestError::ERROR_MISSING_VENDORS);
                $this->setError($err);
                return $this;
            }
            $this->setVendors($vendors);

            return null;
        }

        $vendors = $settings->getVendorIdByVendorKey($vendorKeys);
        if (empty($vendors)) {
            $err = ClientApiRequestError::createError(ClientApiRequestError::ERROR_MISSING_VENDORS);
            $this->setError($err);

            return $this;
        }

        $this->setVendors($vendors);

        return null;
    }


    /**
     * @param array $vendors
     */
    private function setVendors(array $vendors): void
    {
        $this->vendors = $vendors;
    }


    /**
     * @param Settings $settings
     * @param string|null $currencyRates
     * @return ClientApiRequest|null
     */
    private function setCurrencyRatesQuery(Settings $settings, string $currencyRates): ?ClientApiRequest
    {
        $defaultCurrencies = $settings->getDefaultCurrencies();

        $currencyRates = str_replace(' ', '', $currencyRates);

        if (empty($currencyRates)) {
            $this->setRates($defaultCurrencies);
            return null;
        }


        $rates = [];

        $arr = explode(",", strtoupper($currencyRates));

        foreach ($arr as $value) {
            if (!AdapterHelper::isValidCurrency($value)) continue;
            $rates[$value] = $value;
        }

        $rates = empty($rates) ? $defaultCurrencies : $rates;

        $this->setRates($rates);

        return null;
    }

    /**
     * @param int $start
     */
    private function setStart(int $start): void
    {
        $this->start = $start;
    }

    /**
     * @param int $end
     */
    private function setEnd(int $end): void
    {
        $this->end = $end;
    }

    /**
     * @return IClientApiRequestError|null
     */
    public function getError(): ?IClientApiRequestError
    {
        return $this->errors;
    }

    /**
     * @param IClientApiRequestError|null $error
     */
    public function setError(?IClientApiRequestError $error): void
    {
        $this->errors = $error;
    }

    /**
     * @param string|null $key
     * @param Settings $settings
     * @return ClientApiRequest|null
     */
    private function setRequestKey(Settings $settings, ?string $key = null): ?ClientApiRequest
    {
        if (empty($key)) {
            $err = ClientApiRequestError::createError(ClientApiRequestError::ERROR_MISSING_API_KEY);
            $this->setError($err);
            return $this;
        }

        $isValid = $settings->isValidClientKey($key);

        if (false === $isValid) {
            $err = ClientApiRequestError::createError(ClientApiRequestError::ERROR_INVALID_API_KEY);
            $this->setError($err);
            return $this;
        }

        $this->setKey($key);

        return null;
    }

    /**
     * @param Settings $settings
     * @return ClientApiRequest|null
     */
    private function isTimeRequestLimit(Settings $settings): ?ClientApiRequest
    {
        $isTimeRequest = $settings->isTimeRequestLimit($this->getVendorApiKey());
        if ($isTimeRequest) {
            $err = ClientApiRequestError::createError(ClientApiRequestError::ERROR_TOO_MANY_REQUESTS);
            $this->setError($err);
            return $this;
        }

        return null;
    }

    /**
     * @param string $baseCurrency
     * @param Settings $settings
     * @return ClientApiRequest|null
     */
    private function setRequestBase(string $baseCurrency, Settings $settings): ?ClientApiRequest
    {
        $isValidBaseCurrency = $settings->isValidBaseCurrency($baseCurrency);

        if (false === $isValidBaseCurrency) {
            $err = ClientApiRequestError::createError(ClientApiRequestError::ERROR_INVALID_BASE);
            $this->setError($err);
            return $this;
        }

        $this->setBase($baseCurrency);

        return null;
    }

    /**
     * @param int|null $timeStart
     * @param int|null $timeEnd
     * @return void
     */
    private function setRequestTimeRange(?int $timeStart, ?int $timeEnd): void
    {
        [$dateStart, $dateEnd] = DateHelper::getTimeRange($timeStart, $timeEnd);

        $this->setStart($dateStart);
        $this->setEnd($dateEnd);
    }

    /**
     * @param RequestStack $requestStack
     * @param Settings $settings
     * @return ClientApiRequest
     */
    public static function create(RequestStack $requestStack, Settings $settings): ClientApiRequest
    {
        $request = $requestStack->getCurrentRequest();
        $self = new self($request);

        //@NOTE 1) Check client api key
        $err = $self->setRequestKey($settings, $request->get(self::GET_CLIENT_KEY_API));
        if ($err) return $err;

        //@NOTE 2) Check frequency request
        $err = $self->isTimeRequestLimit($settings);
        if ($err) return $err;

        //@NOTE 3) Get base Key
        $err = $self->setRequestBase($request->get(self::GET_CURRENCY_BASE, $settings->getBaseCurrency()), $settings);
        if ($err) return $err;

        //@NOTE 4) Get vendors
        $self->setVendorsQuery($settings, $request->get(self::GET_VENDORS_RATE, ""));

        //@NOTE 5) Get currency Rates
        $err = $self->setCurrencyRatesQuery($settings, $request->get(self::GET_CURRENCY_RATE, ""));
        if ($err) return $err;


        //@NOTE 6) Set time range
        $dateStart = $request->get(self::GET_DATE_START);
        $dateEnd = $request->get(self::GET_DATE_END);

        if ($dateStart) {
            $dateStart = strtotime($dateStart) ?: null;
        }

        if ($dateEnd) {
            $dateEnd = strtotime($dateEnd) ?: null;
        }


        $self->setRequestTimeRange($dateStart, $dateEnd);

        return $self;
    }
}