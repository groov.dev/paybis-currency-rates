<?php
declare(strict_types=1);

namespace Paybis\ExchangeRates\App\Module\ClientApi\Request;

interface IClientApiRequestError
{
    /**
     * @return bool
     */
    public function isInvalidApiKey(): bool;

    /**
     * @return string
     */
    public function getMessage(): string;

    /**
     * @return int
     */
    public function getCode(): int;
}