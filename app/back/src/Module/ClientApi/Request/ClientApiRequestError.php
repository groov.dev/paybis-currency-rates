<?php
declare(strict_types=1);

namespace Paybis\ExchangeRates\App\Module\ClientApi\Request;


class ClientApiRequestError implements IClientApiRequestError
{
    const CODE_NOT_FOUND = 404;
    const CODE_MISSING_API_KEY = 401;
    const CODE_INVALID_API_KEY = 401;
    const CODE_MISSING_VENDORS = 429;
    const CODE_TOO_MANY_REQUESTS = 429;
    const CODE_INVALID_BASE = 400;


    const MESS_NOT_FOUND = "Requested a non-existent resource/route";
    const MESS_MISSING_API_KEY = "Missing an API Key";
    const MESS_INVALID_API_KEY = "Invalid an API Key";
    const MESS_MISSING_VENDORS = "Missing an Vendors for rate";
    const MESS_TOO_MANY_REQUESTS = "Access restricted";
    const MESS_INVALID_BASE = "Requested rates for an unsupported base currency";


    const ERROR_NOT_FOUND = 0;
    const ERROR_MISSING_API_KEY = 1;
    const ERROR_INVALID_API_KEY = 2;
    const ERROR_MISSING_VENDORS = 3;
    const ERROR_TOO_MANY_REQUESTS = 4;
    const ERROR_INVALID_BASE = 5;

    const ERRORS = [
        self::ERROR_NOT_FOUND => [
            'code' => self::CODE_NOT_FOUND,
            'mess' => self::MESS_NOT_FOUND
        ],
        self::ERROR_MISSING_API_KEY => [
            'code' => self::CODE_MISSING_API_KEY,
            'mess' => self::MESS_MISSING_API_KEY
        ],
        self::ERROR_INVALID_API_KEY => [
            'code' => self::CODE_INVALID_API_KEY,
            'mess' => self::MESS_INVALID_API_KEY
        ],
        self::ERROR_MISSING_VENDORS => [
            'code' => self::CODE_MISSING_VENDORS,
            'mess' => self::MESS_MISSING_VENDORS
        ],
        self::ERROR_TOO_MANY_REQUESTS => [
            'code' => self::CODE_TOO_MANY_REQUESTS,
            'mess' => self::MESS_TOO_MANY_REQUESTS
        ],

        self::ERROR_INVALID_BASE => [
            'code' => self::CODE_INVALID_BASE,
            'mess' => self::MESS_INVALID_BASE
        ]
    ];

    /** @var string */
    private $message;

    /** @var int */
    private $code;

    /** @var int */
    private $errorCode = -1;

    /**
     * ErrorRequest constructor.
     * @param string $message
     * @param int $code
     */
    public function __construct(string $message, int $code)
    {
        $this->message = $message;
        $this->code = $code;
    }


    /**
     * @return bool
     */
    public function isInvalidApiKey(): bool
    {
        return self::ERROR_INVALID_API_KEY == $this->errorCode;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @return int
     */
    public function getCode(): int
    {
        return $this->code;
    }


    /**
     * @param int $code
     * @return ClientApiRequestError
     */
    public static function createError(int $code): ClientApiRequestError
    {
        $arr = self::ERRORS[$code] ?? self::ERRORS[0];
        $err = self::create($arr['mess'], $arr['code']);
        $err->errorCode = $code;

        return $err;
    }


    /**
     * @param string $message
     * @param int $code
     * @return ClientApiRequestError
     */
    public static function create(string $message, int $code): ClientApiRequestError
    {
        return new self($message, $code);
    }

}