<?php
declare(strict_types=1);

namespace Paybis\ExchangeRates\App\Module\VendorRates;


interface IVendorRates
{
    /**
     * @return string
     */
    public function getAdapterJson(): string;

    /**
     * @return string
     */
    public function getJson(): string;

    /**
     * @return string
     */
    public function getOriginJson(): string;

    /**
     * @return string
     */
    public function getUrl(): string;

    /**
     * @return string
     */
    public function getVendorKey(): string;

    /**
     * @return string
     */
    public function getBaseCurrency():string;
}