<?php


namespace Paybis\ExchangeRates\App\Module\VendorRates\Opex;

use Paybis\ExchangeRates\App\Helper\AdapterHelper;
use Paybis\ExchangeRates\App\Module\VendorRates\VendorAdapter;

class OpexDataAdapter extends VendorAdapter implements OpexVendorRates
{
    /**
     * @param string $json
     * @return array
     */
    public function getAdaptRates(string $json): array
    {
        $rates = [];
        $arr = json_decode($json, true)['rates'] ?? [];

        foreach ($arr as $key => $val) {

            if (0 == $val) {
                //@TODO Log
                continue;
            }

            $key = (string)$key;
            $val = (float)$val;

            if (!$key || !$val) {
                //@TODO Log
                continue;
            }
            $rates += AdapterHelper::getRatesRow($key, $val);
        }

        return $rates;
    }
}