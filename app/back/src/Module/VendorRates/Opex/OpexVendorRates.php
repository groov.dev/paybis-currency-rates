<?php
declare(strict_types=1);

namespace Paybis\ExchangeRates\App\Module\VendorRates\Opex;

use  Paybis\ExchangeRates\App\Module\VendorRates\IVendorRates;


interface OpexVendorRates extends IVendorRates
{

}