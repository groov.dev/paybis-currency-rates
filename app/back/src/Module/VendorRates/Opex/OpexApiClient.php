<?php
declare(strict_types=1);

namespace Paybis\ExchangeRates\App\Module\VendorRates\Opex;

use Paybis\ExchangeRates\App\Module\VendorRates\VendorRates;
use Symfony\Contracts\HttpClient\HttpClientInterface;


class OpexApiClient extends VendorRates implements OpexVendorRates
{
    /** @var string */
    private $keyApi;

    /**
     * OpenExchangeVendor constructor.
     * @param HttpClientInterface $httpClient
     * @param string $baseUrl
     * @param string $keyApi
     * @param string $vendorKey
     * @param string $base
     */
    public function __construct(HttpClientInterface $httpClient, string $baseUrl,  string $vendorKey, string $base, string $keyApi)
    {
        parent::__construct($httpClient, $baseUrl, $vendorKey, $base);
        $this->keyApi = $keyApi;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return sprintf("%s/latest.json?app_id=%s", $this->apiUrl, $this->keyApi);
    }
}