<?php


namespace Paybis\ExchangeRates\App\Module\VendorRates\Opex;

use Paybis\ExchangeRates\App\Module\VendorRates\VendorValidator;
use Symfony\Component\Validator\Constraints\Composite;
use Symfony\Component\Validator\Constraints as Assert;


class OpexDataValidator extends VendorValidator implements OpexVendorRates
{
    /**
     * @return Composite
     */
    public function getInputConstraint(): Composite
    {
        return new Assert\Collection([
            'disclaimer' => [
                new Assert\NotBlank(),
                new Assert\Type("string")
            ],
            'license' => [
                new Assert\NotBlank(),
                new Assert\Type("string")
            ],
            'timestamp' => [
                new Assert\NotBlank(),
                new Assert\Type("integer")
            ],
            'base' => [
                new Assert\NotBlank(),
                new Assert\Length(['min' => 3, 'max' => 3]),
                new Assert\EqualTo('USD'),
            ],
            'rates' => new Assert\Optional([
                new Assert\Type('array'),
                new Assert\Count(['min' => 5]),
                new Assert\All(
                    [
                        'constraints' => [
                            new Assert\NotBlank(),
                            new Assert\Type("numeric")
                        ]]),
            ])
        ]);
    }

    /**
     * @param string $originJson
     * @return array|null
     */
    public function validateInputJsonByErrors(string $originJson): ?array
    {
        //@TODO 1) Validate answer data as valid JSON format
        $inputData = json_decode($originJson, true);

        if (!is_array($inputData) || !count($inputData)) {
            //@TODO Log error
            return null;
        }
        //@TODO 2) Validate answer data for current vendor API errors

        return $inputData;
    }
}