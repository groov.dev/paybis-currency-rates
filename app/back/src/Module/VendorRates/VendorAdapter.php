<?php
declare(strict_types=1);

namespace Paybis\ExchangeRates\App\Module\VendorRates;


use Paybis\ExchangeRates\App\Helper\AdapterHelper;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

abstract class VendorAdapter extends VendorDecorator
{
    /**
     * @return string
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function getAdapterJson(): string
    {
        $adaptRates = $this->getAdaptRates($this->getOriginJson());

        return AdapterHelper::getAdapterJson($this->getBaseCurrency(), $adaptRates);
    }

    abstract protected function getAdaptRates(string $json): array;
}