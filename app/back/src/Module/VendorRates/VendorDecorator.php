<?php
declare(strict_types=1);

namespace Paybis\ExchangeRates\App\Module\VendorRates;

use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;


abstract class VendorDecorator implements IVendorRates
{
    /** @var IVendorRates */
    protected $vendorRates;

    /**
     * VendorApiAdapter constructor.
     * @param $vendorRates
     */
    public function __construct(IVendorRates $vendorRates)
    {
        $this->vendorRates = $vendorRates;
    }

    /**
     * @return string
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
     public function getAdapterJson(): string
     {
         return $this->vendorRates->getAdapterJson();
     }

     public function getJson(): string
    {
        return $this->vendorRates->getJson();
    }

    /**
     * @return string
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function getOriginJson(): string
    {
        return $this->vendorRates->getOriginJson();
    }

    public function getUrl(): string
    {
        return $this->vendorRates->getUrl();
    }

    public function getVendorKey(): string
    {
        return $this->vendorRates->getVendorKey();
    }

    public function getBaseCurrency(): string
    {
        return $this->vendorRates->getBaseCurrency();
    }
}