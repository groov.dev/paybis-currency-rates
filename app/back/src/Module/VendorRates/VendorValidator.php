<?php
declare(strict_types=1);

namespace Paybis\ExchangeRates\App\Module\VendorRates;

use Paybis\ExchangeRates\App\Settings\Settings;
use Symfony\Component\Validator\Constraints\Composite;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Class VendorValidator
 * @package Paybis\ExchangeRates\App\Module\VendorRates
 */
abstract class VendorValidator extends VendorDecorator
{
    /** @var IVendorRates */
    protected $vendorRates;

    /** @var Settings $settings */
    protected $settings;

    /**
     * VendorValidator constructor.
     * @param IVendorRates $vendorRates
     * @param Settings $settings
     */
    public function __construct(IVendorRates $vendorRates, Settings $settings)
    {
        parent::__construct($vendorRates);

        $this->settings = $settings;
    }

    /**
     * @return string
     */
    public function getAdapterJson(): string
    {
        $json = $this->vendorRates->getAdapterJson();

        $inputStruct = json_decode($json, true);
        $inputConstraint = $this->getOutputConstraint();

        $errors = $this->validateStruct($inputStruct, $inputConstraint);

        //If Error we can't store to DB
        //Store to log
        if (!$errors) return $json;

        $this->settings->logErrorVendorRatesOutputData($this->vendorRates, $errors);

        return "";
    }

    /**
     * @return Composite
     */
    public function getOutputConstraint(): Composite
    {
        return new Assert\Collection([
            'base' => [
                new Assert\NotBlank(),
                new Assert\Length(['min' => 3, 'max' => 3]),
            ],
            'rates' => new Assert\Optional([
                new Assert\Type('array'),
                new Assert\Count(['min' => 5]),
                new Assert\All(
                    [
                        'constraints' => [
                            new Assert\NotBlank(),
                            new Assert\Type("numeric")
                        ]]),
            ])
        ]);
    }

    /**
     * @param ConstraintViolationList $viola
     * @return string|null
     */
    private function getValidateError(ConstraintViolationList $viola): ?string
    {
        if ($viola->count() == 0) return null;

        $err = [];
        $i = 0;
        foreach ($viola as $violation) {
            $err[$i]['message'] = (string)$violation;
            $err[$i]['value'] = var_export($violation->getInvalidValue(), true);
            $i++;
        }

        return json_encode($err);
    }

    /**
     * @param array $inputStruct
     * @param Composite $inputConstraint
     * @return string|null
     */
    protected function validateStruct(array $inputStruct, Composite $inputConstraint): ?string
    {
        $validator = Validation::createValidator();

        /** @var ConstraintViolationList $viola */
        $viola = $validator->validate($inputStruct, $inputConstraint);

        return $this->getValidateError($viola);
    }

    /**
     * @param array $inputStruct
     * @param Composite $inputConstraint
     */
    protected function validateInputStruct(array $inputStruct, Composite $inputConstraint): void
    {
        $errors = $this->validateStruct($inputStruct, $inputConstraint);

        //@TODO Create logger
        $this->settings->logErrorVendorRatesInputData($this->vendorRates, $errors);
    }

    /**
     * @return string
     */
    public function getOriginJson(): string
    {
        $originJson = $this->vendorRates->getOriginJson();

        $inputData = $this->validateInputJsonByErrors($originJson);

        if (!$inputData) return $originJson;

        $this->validateInputStruct($inputData, $this->getInputConstraint());

        return $originJson;
    }

    /**
     * @return Composite
     */
    abstract public function getInputConstraint(): Composite;

    /**
     * @param string $originJson
     * @return array|null
     */
    abstract public function validateInputJsonByErrors(string $originJson): ?array;
}