<?php
declare(strict_types=1);

namespace Paybis\ExchangeRates\App\Module\VendorRates;


use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

abstract class VendorRates implements IVendorRates
{
    /**
     * @var bool
     */
    protected $isActive = true;

    /** @var HttpClientInterface */
    protected $httpClient;

    /** @var string */
    protected $vendorKey;

    /** @var string */
    protected $apiUrl;

    /** @var string */
    protected $base;


    /**
     * VendorApi constructor.
     * @param HttpClientInterface $httpClient
     * @param string $apiUrl
     * @param string $vendorKey
     * @param string $base
     */
    public function __construct(HttpClientInterface $httpClient, string $apiUrl, string $vendorKey, string $base)
    {
        $this->httpClient = $httpClient;
        $this->vendorKey = $vendorKey;
        $this->apiUrl = $apiUrl;
        $this->base = $base;
    }
    /**
     * @return string
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function getAdapterJson(): string
    {
        return $this->getOriginJson();
    }

    /**
     * @param string $url
     * @return string
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    protected function getApiData(string $url): string
    {
        $response = $this->httpClient->request('GET', $url);

        return $response->getContent();
    }

    /**
     * @return string
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function getOriginJson(): string
    {
        return $this->getApiData($this->getUrl());
    }

    /**
     * @return string
     */
    public function getJson(): string
    {
        return $this->getAdapterJson();
    }


    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->isActive;
    }

    /**
     * @param bool $isActive
     */
    public function setIsActive(bool $isActive): void
    {
        $this->isActive = $isActive;
    }

    /**
     * @return string
     */
    public function getVendorKey(): string
    {
        return $this->vendorKey;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->apiUrl;
    }

    /**
     * @return string
     */
    public function getBaseCurrency(): string
    {
        return $this->base;
    }
}