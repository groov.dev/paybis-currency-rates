<?php
declare(strict_types=1);

namespace Paybis\ExchangeRates\App\Module\VendorRates\Binfo;

use  Paybis\ExchangeRates\App\Module\VendorRates\IVendorRates;

interface  BinfoVendorRates extends IVendorRates
{

}