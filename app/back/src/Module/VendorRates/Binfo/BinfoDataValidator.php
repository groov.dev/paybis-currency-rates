<?php
declare(strict_types=1);

namespace Paybis\ExchangeRates\App\Module\VendorRates\Binfo;


use Paybis\ExchangeRates\App\Module\VendorRates\VendorValidator;
use Symfony\Component\Validator\Constraints\Composite;
use Symfony\Component\Validator\Constraints as Assert;


class BinfoDataValidator extends VendorValidator implements BinfoVendorRates
{

    /**
     * @return Composite
     */
    public function getInputConstraint(): Composite
    {
        return new Assert\All(['constraints' => [
            new Assert\Collection([
                '15m' => [new Assert\NotBlank(), new Assert\Type("numeric")],
                'last' => [new Assert\NotBlank(), new Assert\Type("numeric")],
                'buy' => [new Assert\NotBlank(), new Assert\Type("numeric")],
                'sell' => [new Assert\NotBlank(), new Assert\Type("numeric")],
                'symbol' => [new Assert\NotBlank(), new Assert\Length(['min' => 3, 'max' => 3]), new Assert\Type("string")],
            ])
        ]]);
    }


    /**
     * @param string $originJson
     * @return array|null
     */
    public function validateInputJsonByErrors(string $originJson): ?array
    {
        //@TODO 1) Validate answer data as valid JSON format
        $inputData = json_decode($originJson, true);

        if (!is_array($inputData) || !count($inputData)) {
            //@TODO Log error
            return null;
        }

        //@TODO 2) Validate answer data for current vendor API errors

        return $inputData;
    }
}