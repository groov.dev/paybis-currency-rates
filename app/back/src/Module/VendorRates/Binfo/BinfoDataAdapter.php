<?php
declare(strict_types=1);

namespace Paybis\ExchangeRates\App\Module\VendorRates\Binfo;


use Paybis\ExchangeRates\App\Helper\AdapterHelper;
use Paybis\ExchangeRates\App\Module\VendorRates\VendorAdapter;


class BinfoDataAdapter extends VendorAdapter implements BinfoVendorRates
{
    /**
     * @param string $json
     * @return array
     */
    protected function getAdaptRates(string $json): array
    {
        $rates = [];
        $arr = json_decode($json, true) ?? [];

        foreach ($arr as $row) {
            $key = $row["symbol"] ?? null;
            $val = $row["last"] ?? null;

            if (is_null($key) || is_null($val)) {
                //@TODO Log
                continue;
            }

            if (0 == $val) {
                //@TODO Log
                continue;
            }

            $key = (string)$key;
            $val = (float)$val;

            if (!$key || !$val) {
                //@TODO Log
                continue;
            }

            $rates += AdapterHelper::getRatesRow($key, $val);
        }
        return $rates;
    }
}