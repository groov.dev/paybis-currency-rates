<?php
declare(strict_types=1);

namespace Paybis\ExchangeRates\App\Module\VendorRates\Bitpay;

use  Paybis\ExchangeRates\App\Module\VendorRates\IVendorRates;

interface BitpayVendorRates extends IVendorRates
{

}