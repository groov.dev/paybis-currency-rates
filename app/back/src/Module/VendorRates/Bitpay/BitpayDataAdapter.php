<?php


namespace Paybis\ExchangeRates\App\Module\VendorRates\Bitpay;


use Paybis\ExchangeRates\App\Helper\AdapterHelper;

use Paybis\ExchangeRates\App\Module\VendorRates\VendorAdapter;


class BitpayDataAdapter extends VendorAdapter implements BitpayVendorRates
{
    /**
     * @param string $json
     * @return array
     */
    public function getAdaptRates(string $json): array
    {
        $rates = [];

        foreach (json_decode($json, true) as $row) {
            $key = $row["code"] ?? null;
            $val = $row["rate"] ?? null;

            if (is_null($key) || is_null($val)) {
                //@TODO Log
                continue;
            }

            if (0 == $val) {
                //@TODO Log
                continue;
            }

            $key = (string)$key;
            $val = (float)$val;

            if (!$key || !$val) {
                //@TODO Log
                continue;
            }

            $rates += AdapterHelper::getRatesRow($key, $val);
        }

        return $rates;
    }
}