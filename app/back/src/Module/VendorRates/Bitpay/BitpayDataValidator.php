<?php


namespace Paybis\ExchangeRates\App\Module\VendorRates\Bitpay;


use Paybis\ExchangeRates\App\Module\VendorRates\VendorValidator;
use Symfony\Component\Validator\Constraints\Composite;
use Symfony\Component\Validator\Constraints as Assert;


class BitpayDataValidator extends VendorValidator implements BitpayVendorRates
{
    /**
     * @return Composite
     */
    public function getInputConstraint(): Composite
    {
        return new Assert\All(['constraints' => [
            new Assert\Collection([
                'code' => [new Assert\NotBlank(), new Assert\Length(['min' => 3, 'max' => 4]), new Assert\Type("string")],
                'name' => [new Assert\NotBlank(), new Assert\Type("string")],
                'rate' => [new Assert\NotBlank(), new Assert\Type("numeric")],
            ])
        ]]);
    }

    /**
     * @param string $originJson
     * @return array|null
     */
    public function validateInputJsonByErrors(string $originJson): ?array
    {
        //@TODO 1) Validate answer data as valid JSON format
        $inputData = json_decode($originJson, true);

        if (!is_array($inputData) || !count($inputData)) {
            //@TODO Log error
            return null;
        }
        //@TODO 2) Validate answer data for current vendor API errors

        return $inputData;
    }
}