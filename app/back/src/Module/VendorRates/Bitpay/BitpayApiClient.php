<?php
declare(strict_types=1);

namespace Paybis\ExchangeRates\App\Module\VendorRates\Bitpay;

use Paybis\ExchangeRates\App\Module\VendorRates\VendorRates;


class BitpayApiClient extends VendorRates implements BitpayVendorRates
{

}