<?php
declare(strict_types=1);

namespace Paybis\ExchangeRates\App\Settings;


use Doctrine\Bundle\DoctrineBundle\Registry;
use Paybis\ExchangeRates\App\Helper\DateHelper;
use Paybis\ExchangeRates\App\Helper\QueryHelper;
use Paybis\ExchangeRates\App\Module\ClientApi\Request\IClientApiRequest;
use Paybis\ExchangeRates\App\Module\VendorRates\IVendorRates;
use Paybis\ExchangeRates\App\Storages\PG\Entity\ClientApi;
use Paybis\ExchangeRates\App\Storages\PG\Entity\VendorDataLog;
use Paybis\ExchangeRates\App\Storages\PG\Entity\VendorList;
use Paybis\ExchangeRates\App\Storages\PG\Entity\VendorRates;
use Paybis\ExchangeRates\App\Storages\PG\Repository\VendorRatesRepository;
use Paybis\ExchangeRates\App\Storages\Redis\PaybisRedis;
use Paybis\ExchangeRates\App\Storages\PG\Repository\VendorListRepository;
use Doctrine\DBAL\Driver\Exception as DriverException;
use Doctrine\DBAL\Exception as DBALException;

/**
 * Class Settings
 * @package Paybis\ExchangeRates\App\Settings
 */
class Settings implements ISettings
{

    /** @var CurrencySettings */
    private $currencies;

    /** @var Registry */
    private $registry;

    /** @var PaybisRedis */
    private $paybisRedis;

    /**
     * Settings constructor.
     * @param CurrencySettings $currencies
     * @param Registry $registry
     * @param PaybisRedis $paybisRedis
     */
    public function __construct(CurrencySettings $currencies, Registry $registry, PaybisRedis $paybisRedis)
    {
        $this->currencies = $currencies;
        $this->registry = $registry;
        $this->paybisRedis = $paybisRedis;
    }

    /**
     * @return VendorList[]
     */
    public function getAllVendors(): array
    {
        return $this->vendorList()->findAll();
    }

    public function getAllVendorsByKey(): array
    {
        $keys = [];
        $vendors = $this->getActiveVendors();
        /** @var VendorList $vendorList */
        foreach ($vendors as $vendorList) {
            $keys[$vendorList->getKey()] = $vendorList->getId();
        }

        return $keys;
    }

    /**
     * @param array $keys
     * @return array
     */
    public function getVendorIdByVendorKey(array $keys): array
    {
        $data = [];
        $vendorsKey = $this->getActiveVendorsKey();

        foreach ($keys as $key) {
            /** @var VendorList $vendor */
            $vendor = $vendorsKey[$key] ?? null;
            if (!$vendor) continue;
            $data[$key] = $vendor->getId();
        }

        return $data;
    }

    public function getActiveVendorsKey(): array
    {
        $keys = [];
        $vendors = $this->getActiveVendors();
        /** @var VendorList $vendorList */
        foreach ($vendors as $vendorList) {
            $keys[$vendorList->getKey()] = $vendorList;
        }

        return $keys;
    }

    /**
     * @return array
     */
    public function getActiveVendors(): array
    {
        return $this->vendorList()->getVendorsByActive();
    }

    /**
     * @return array
     */
    public function getDefaultCurrencies(): array
    {
        return $this->currencies->getDefaultCurrencies();
    }

    /**
     * @return string
     */
    public function getBaseCurrency(): string
    {
        return $this->currencies->getBaseCurrency();
    }

    public function isValidBaseCurrency(string $base): bool
    {
        return $this->currencies->isValidBaseCurrency($base);
    }


    /**
     * @param int $vendorId
     * @return VendorList|null
     */
    public function getVendorById(int $vendorId): ?VendorList
    {
        return $this->vendorList()->find($vendorId) ?: null;
    }

    /**
     * @param string $vendorKey
     * @return VendorList|null
     */
    public function getVendorByKey(string $vendorKey): ?VendorList
    {
        return $this->vendorList()->findOneBy(["key" => $vendorKey]) ?: null;
    }

    /**
     * @param int|null $statTime
     * @return int
     */
    public function getTimeStart(int $statTime = null): int
    {
        return DateHelper::getTimeStart($statTime);
    }

    /**
     * @param int|null $endTime
     * @return int
     */
    public function getTimeEnd(int $endTime = null): int
    {
        return DateHelper::getTimeEnd($endTime);
    }

    /**
     * @param int|null $statTime
     * @param int|null $endTime
     * @return array
     */
    public function getTimeRange(int $statTime = null, int $endTime = null): array
    {
        return DateHelper::getTimeRange($statTime, $endTime);
    }

    /**
     * @param IClientApiRequest $request
     * @return array
     * @throws DriverException
     * @throws DBALException
     */
    public function getCurrencyRates(IClientApiRequest $request): array
    {
        return $this->vendorRates()->getCurrencyRates($request);
    }

    /**
     * @return VendorListRepository
     */
    private function vendorList(): VendorListRepository
    {
        return $this->registry->getRepository(VendorList::class);
    }

    /**
     * @return VendorRatesRepository
     */
    private function vendorRates(): VendorRatesRepository
    {
        return $this->registry->getRepository(VendorRates::class);
    }


    /**
     * @return int
     */
    public function daemonSleep(): int
    {
        return $this->paybisRedis->daemonSleep();
    }

    /**
     * @return bool
     */
    public function daemonIsTurnOff(): bool
    {
        return $this->paybisRedis->daemonIsTurnOff();
    }

    /**
     * Update key with TTL that indicate alive daemon
     */
    public function daemonIsAliveUpdate(): void
    {
        $this->paybisRedis->daemonIsAliveUpdate();
    }

    /**
     * @return bool
     */
    public function daemonIsAlive(): bool
    {
        return $this->paybisRedis->daemonIsAlive();
    }

    /**
     * @param string $apiKey
     * @param int|null $ttl
     * @return bool
     */
    public function isTimeRequestLimit(string $apiKey, int $ttl = null): bool
    {
        return $this->paybisRedis->isTimeRequestLimit($apiKey);
    }

    /**
     * @param string $vendorKey
     * @return bool
     */
    public function isValidVendor(string $vendorKey): bool
    {
        $vendorKey = strtolower($vendorKey);
        if (QueryHelper::ALL == $vendorKey) return true;

        $vendor = $this->getVendorByKey($vendorKey);

        if (!is_object($vendor)) return false;


        return $vendor->isActive();
    }

    /**
     * @param string $apiKey
     * @return bool
     */
    public function isValidClientKey(string $apiKey): bool
    {
        //Check in Redis
        $isValid = $this->paybisRedis->isValidClientKey($apiKey);

        if (null !== $isValid) return $isValid;

        //If key null  Check in Redis in DB
        $isValid = $this->registry->getRepository(ClientApi::class)->isValidApiKey($apiKey);

        $val = $isValid ? PaybisRedis::VALID : PaybisRedis::INVALID;

        //Set value to 1 hour
        $this->paybisRedis->setWithApiKey($apiKey, $val, DateHelper::SEC_HOUR);

        return $isValid;
    }

    /**
     * @return PaybisRedis
     */
    public function getRedis(): PaybisRedis
    {
        return $this->paybisRedis;
    }

    /**
     * @param IVendorRates $vendor
     * @param string $errors
     */
    public function logErrorVendorRatesInputData(IVendorRates $vendor, string $errors): void
    {
        $this->paybisRedis->logErrorVendorRatesInputData($vendor->getVendorKey(), $errors);
    }

    /**
     * @param IVendorRates $vendor
     * @param string $errors
     */
    public function logErrorVendorRatesOutputData(IVendorRates $vendor, string $errors): void
    {
        $timeId = DateHelper::getHourTs();
        $vendorList = $this->getVendorByKey($vendor->getVendorKey());
        if (!$vendorList) return;

        $originJson = $vendor->getOriginJson();

        $this->registry->getRepository(VendorDataLog::class)->logErrorVendorRatesOutputData($vendorList, $originJson, $errors, $timeId);
    }
}