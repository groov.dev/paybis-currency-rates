<?php


namespace Paybis\ExchangeRates\App\Settings;


/**
 * Interface ICurrencySettings
 * @package Paybis\ExchangeRates\App\Settings
 */
interface ICurrencySettings
{
    /**
     * @return array
     */
    public function getDefaultCurrencies(): array;

    /**
     * @return string
     */
    public function getBaseCurrency(): string;

    /**
     * @param string $base
     * @return bool
     */
    public function isValidBaseCurrency(string $base): bool;
}