<?php
declare(strict_types=1);

namespace Paybis\ExchangeRates\App\Settings;


use Paybis\ExchangeRates\App\Helper\AdapterHelper;

/**
 * Class CurrencySettings
 * @package Paybis\ExchangeRates\App\Settings
 */
class CurrencySettings implements ICurrencySettings
{
    /**
     * Default currency for rates
     */
    const CURR_BASE = 'BTC';

    /**
     * Default currency for system work
     */
    const CURR_DEFAULT = ['BTC', 'USD', 'EUR', 'GBP', 'JPY'];

    /** @var array */
    private $defaultCurrencies;

    /** @var string */
    private $baseCurrency;

    /**
     * CurrencySettings constructor.
     * @param array $defaultCurrencies
     * @param string $baseCurrency
     */
    private function __construct(array $defaultCurrencies, string $baseCurrency)
    {
        $this->defaultCurrencies = $defaultCurrencies;
        $this->baseCurrency = $baseCurrency;
    }

    /**
     * @return array
     */
    public function getDefaultCurrencies(): array
    {
        return $this->defaultCurrencies;
    }

    /**
     * @return string
     */
    public function getBaseCurrency(): string
    {
        return $this->baseCurrency;
    }


    /**
     * @param array $currencies
     * @return CurrencySettings
     */
    public static function create(array $currencies): CurrencySettings
    {
        $base = $currencies["base"] ?? "";

        $base = AdapterHelper::isValidCurrency($base) ? strtoupper($base) : CurrencySettings::CURR_BASE;


        $default = $currencies["default"] ?? [];

        if (count($default)) {
            $default = AdapterHelper::arrayConfigFormat($default);
            if (count($default)) {
                $default = array_filter($default, [AdapterHelper::class, 'isValidCurrency']);
            }
        }

        if (empty($default)) {
            $default = AdapterHelper::arrayKeyVal(self::CURR_DEFAULT);
        }

        return new self($default, $base);
    }

    /**
     * @param string $base
     * @return bool
     */
    public function isValidBaseCurrency(string $base): bool
    {
        return isset($this->getDefaultCurrencies()[$base]);
    }
}