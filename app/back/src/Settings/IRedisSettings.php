<?php


namespace Paybis\ExchangeRates\App\Settings;


/**
 * Interface IRedisSettings
 * @package Paybis\ExchangeRates\App\Settings
 */
interface IRedisSettings
{

    /**
     * void
     */
    public function daemonIsAliveUpdate(): void;

    /**
     * @return bool
     */
    public function daemonIsAlive(): bool;

    /**
     * @return int
     */
    public function daemonSleep(): int;

    /**
     * @return bool
     */
    public function daemonIsTurnOff(): bool;

    /**
     * @param string $apiKey
     * @return bool|null
     */
    public function isValidClientKey(string $apiKey): ?bool;

    /**
     * @param string $apiKey
     * @param int|null $ttl
     * @return bool
     */
    public function isTimeRequestLimit(string $apiKey, int $ttl = null): bool;
}