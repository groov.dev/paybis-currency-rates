<?php

declare(strict_types=1);

namespace Paybis\ExchangeRates\App\Settings;


/**
 * Interface ISettings
 * @package Paybis\ExchangeRates\App\Settings
 */
interface ISettings extends ICurrencySettings, IProjectSettings, IRedisSettings
{

}