<?php

declare(strict_types=1);

namespace Paybis\ExchangeRates\App\Settings;


use Paybis\ExchangeRates\App\Storages\PG\Entity\VendorList;

/**
 * Interface IProjectSettings
 * @package Paybis\ExchangeRates\App\Settings
 */
interface IProjectSettings
{
    /**
     * @return array
     */
    public function getAllVendors(): array;

    /**
     * @return array
     */
    public function getActiveVendors(): array;

    /**
     * @param int $vendorId
     * @return VendorList|null
     */
    public function getVendorById(int $vendorId): ?VendorList;

    /**
     * @param string $vendorKey
     * @return VendorList|null
     */
    public function getVendorByKey(string $vendorKey): ?VendorList;

    /**
     * @param int|null $statTime
     * @return int
     */
    public function getTimeStart(int $statTime = null): int;

    /**
     * @param int|null $endTime
     * @return int
     */
    public function getTimeEnd(int $endTime = null): int;

    /**
     * @param int|null $statTime
     * @param int|null $endTime
     * @return array
     */
    public function getTimeRange(int $statTime = null, int $endTime = null): array;
}