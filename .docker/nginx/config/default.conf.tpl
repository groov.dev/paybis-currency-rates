    server {
        charset utf-8;
        client_max_body_size 1024M;

        error_log  /var/log/nginx/nginx_errors.log;
        access_log /var/log/nginx/nginx_access.log;

        listen 8080;
        server_name ${NGINX_VHOST_FRONT};

        root /var/www/html/front/public;
        index index.html;


        location / {
            index  index.html;
        }


    # redirect server error pages to the static page /50x.html /var/www/html/front/public
        #
        error_page 500 502 503 504 /50x.html;
        location = /50x.html {
            root /var/www/html/front/error;
        }

        location /api {
                auth_basic off;
                proxy_pass http://paybis_nginx:8443;
                proxy_http_version 1.1;
                proxy_buffering     off;
                proxy_max_temp_file_size 0;
                proxy_redirect     default;
                proxy_set_header   Host             $host;
                proxy_set_header   X-Real-IP        $remote_addr;
                proxy_set_header   X-Forwarded-For  $proxy_add_x_forwarded_for;
                proxy_set_header   Connection       "";
        }

        # deny access to . files, for security
        #
        location ~ /\. {
            log_not_found off;
            deny all;
        }
    }

    server {
        charset utf-8;
        client_max_body_size 1024M;

        error_log  /var/log/nginx/php_error.log;
        access_log /var/log/nginx/php_access.log;

        listen 8443;
        server_name ${NGINX_VHOST_BACK};

        root /var/www/html/back/public;
        index index.php;

        location / {
            try_files $uri /index.php$is_args$args;
        }

        location ~ ^/index\.php(/|$) {
            fastcgi_pass upstream_php;
            fastcgi_split_path_info ^(.+\.php)(/.*)$;
            include fastcgi_params;
            fastcgi_index index.php;
            fastcgi_buffer_size 32k;
            fastcgi_buffers 4 32k;

            fastcgi_param SCRIPT_FILENAME $realpath_root$fastcgi_script_name;
            fastcgi_param DOCUMENT_ROOT $realpath_root;
            internal;
        }
    }