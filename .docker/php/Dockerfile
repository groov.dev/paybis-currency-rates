FROM php:8.0.3-fpm-alpine

MAINTAINER Groov Development <groov.dev@gmail.com>

ARG ERROR_REPORTING=-1
ENV ERROR_REPORTING=$ERROR_REPORTING

ARG DATE_TIMEZONE=UTC
ENV DATE_TIMEZONE=$DATE_TIMEZONE

ARG DISPLAY_ERRORS=On
ENV DISPLAY_ERRORS=$DISPLAY_ERRORS


COPY ./config/php.ini /usr/local/etc/php/php.ini
COPY ./config/php-fpm.conf /usr/local/etc/php-fpm.conf
COPY ./config/php-fpm.d/zz-docker.conf /usr/local/etc/php-fpm.d/zz-docker.conf


ENV PHP_SETS  \
     php8-soap \
     php8-zip \
     php8-bcmath \
     php8-dom \
     php8-common \
     php8-intl \
     php8-pgsql \
     php8-redis \
     php8-curl

RUN apk --no-cache add pcre-dev postgresql-dev ${PHPIZE_DEPS} \
  && pecl install redis \
  && docker-php-ext-install pdo pdo_mysql pdo_pgsql \
  && docker-php-ext-enable redis \
  && apk del pcre-dev ${PHPIZE_DEPS} \
  && apk --no-cache add ${PHP_SETS}

EXPOSE 9000
CMD ["php-fpm"]