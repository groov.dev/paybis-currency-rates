## Currency rates API

### An  example on Symphony

### Task

- It is necessary to develop a web application that will provide API for Bitcoin cryptocurrency exchange rate: BTC /
  USD, BTC / EUR etc.
- The API must allow the exchange rate of the currency on the clock and be able to change the output range.
- The application must have a functional periodic renewal currency exchange rates with a real exchange.
- The number provided through the API pairs of currencies is not less than 3.

## Installation ##

### Docker

The simple way to run app in the Docker containers.

* [Install Docker][1]
* [Install Docker Compose][2]

### Setup .env

After install docker need to setup environment variables.  
For example create .env files. You can copy it.

```shell
cp .env.tpl .env
```

```shell
cp app/back/.env.tpl app/back/.env
```

### Start docker container

Running docker containers using docker-compose

```shell
docker-compose up --build -d
```

### Check docker container status

```shell
docker ps
```

### Stop docker container

```shell
docker-compose down -v
```

### Composer
Don't forget run composer install into your php container. 
If you want run CLI command from host mashine, maybe you will need to add contaner name to your /etc/hosts file.
```shell
composer install
```
### Setup db

Now create DB

```shell
php bin/console doctrine:database:create
```

Now create db tables

```shell
php bin/console doctrine:migrations:migrate
```

The repository has a dump file with test data. It is not necessary. But for quick test you can load it.

```shell
psql -U postgres -c 'drop database paybis;'
psql -U postgres -c 'create database paybis;'
psql -U postgres paybis < dump.sql
```

### Start daemons

Command for update db periodic renewal currency exchange rates. At the moment there is one data provider: OpenExchange.

```shell
php bin/console currency-rate:vendor-open-exchange:get-latest-exec
```

It collects data from the provider once an hour. Checks every ten minutes if an hour has passed.

### Usage

There are now four links for the test.

- Info link

```shell
http://localhost:8080/api/info
```

- Not found link

```shell
http://localhost:8080/api/aaa/bbb
```

- Latest link

```shell
http://localhost:8080/api?vendors=all&key={api_key}
```

- Time range (period) [2021-07-28, 2021-07-29], vendors [binfo,opex], base=[GBP]

```shell
http://localhost:8080/api?vendors=binfo,opex&base=GBP&start=2021-07-28&end=2021-07-29&key={api_key}
```
To get the data, you need a registered user with API key.
If you imported the dump file, then there are already two registered users with keys.
- key: af1eea39d7b32a12a5c75439674d71085ac006a487f0acc318aea41cf4d0f2f8
- key: ac63227f09590a0c4383daa7c0008cf9e15f9ca219e02bdb92523b9693ec3c30

Otherwise, you need to register the user. Manually write the data into the table 'Users'.

### Usage examples 
- Latest  
All fields as default values
```shell
http://localhost:8080/api?vendors=all&key=af1eea39d7b32a12a5c75439674d71085ac006a487f0acc318aea41cf4d0f2f8
```  
Change BASE from BTC to GBP
```shell
http://localhost:8080/api?vendors=all&base=GBP&key=af1eea39d7b32a12a5c75439674d71085ac006a487f0acc318aea41cf4d0f2f8
```
Change BASE from BTC to EUR and get only BTC, USD, JPY
```shell
http://localhost:8080/api?vendors=opex,binfo&base=EUR&rates=btc,usd,jpy&key=af1eea39d7b32a12a5c75439674d71085ac006a487f0acc318aea41cf4d0f2f8
```

- Period
 Same as "Latest" request, but having time range "start" and "end"
  There are several different behaviors. Depending on the set values "start" and "end"   
  By default "end" is Now()  and "start" is now() - day. But it can set hour too.  
 - For example start=2021-07-29T11:30:00   and end=2021-07-29T18:30:00
```shell
 http://localhost:8080/api?start=2021-07-29T11:30:00&end=2021-07-29T18:30:00&vendors=opex&base=EUR&rates=btc,usd,jpy&key=af1eea39d7b32a12a5c75439674d71085ac006a487f0acc318aea41cf4d0f2f8  
```

[1]:  https://docs.docker.com/engine/install/

[2]:  https://docs.docker.com/compose/install/
